<?php

\OC::$server->getEventDispatcher()->addListener('OCA\DAV\Connector\Sabre::addPlugin', function(\OCP\SabrePluginEvent $event) {
	$event->getServer()->addPlugin(new \OCA\DAV\Connector\Sabre\CorsPlugin(\OC::$server->getConfig()));
});
